#!/bin/python3
import cv2

image = cv2.imread("splash/bootsplash.jpg")

image_bgr = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

cv2.imwrite("splash/bootsplash.jpg", image_bgr)
