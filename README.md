# coreboot-x220

This is a fairly standard Coreboot configuration, with a splash screen and a couple of extra utility scripts included specifically for a Thinkpad X220.

This repository is a fork of https://review.coreboot.org/coreboot.git.

## Helpful Commands

- `make nconfig`: `ncurses` interface for Coreboot configuration
- `make`: Generate the Coreboot ROM image and write it to `build/coreboot.rom`

### Note: `make` must be run after any configuration changes in order to update the Coreboot ROM image. This should be followed by `util/flash.sh` to write this image to ROM.

## Guides For Flashing Coreboot The First Time

- [Official](https://www.coreboot.org/Board:lenovo/x220)
- [Karl Cordes](https://karlcordes.com/coreboot-x220)

## Internally Flashing Subsequent Coreboot Builds

A couple of utility scripts are included for reading/writing the X220's ROM chip:

- `util/backup.sh` will read the current ROM contents and write them to `build/backup.rom`
- `util/flash.sh` will write the `build/coreboot.rom` image to ROM. **Only use after running `make`**

## Editing The Bootsplash Image

There are a few requirements for a Coreboot/SeaBIOS bootsplash image:

- The width and height of the image must both be divisible by `16`. In the case of the `1366x768` X220 display, the closest compatible resolution is `1360x768`.
- The image must be a JPEG with `4:2:0` subsampling. If exporting from GIMP, the `progressive` export option must be unchecked as well.

A GIMP project and a compatible JPEG are located in the `splash` directory. If a new version of the bootsplash image is exported from GIMP, the `util/convert_rgb.py` script must be run the flip the RGB channels in order to display properly with coreboot.
